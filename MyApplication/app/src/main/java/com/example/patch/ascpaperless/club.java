package com.example.patch.ascpaperless;

import org.json.JSONObject;

/**
 * Created by David Marchant on 15/03/2016.
 *
 * THis is the class description for the club class. it is generated from the JSON
 */
public class club
{
    private String title;
    private String startAt;
    private String endAt;

    public void setTitle(String title)
    {
        if (title != null)
        {
            this.title = title;
        }
        else
        {
            throw new ClassCastException("Club name cannot cannot be null");
        }
    }

    public String getTitle()
    {
        return title;
    }

    public void setStartAt(String startAt)
    {
        if (startAt != null)
        {
            this.startAt = startAt;
        }
        else
        {
            throw new ClassCastException("Club starting time cannot be null");
        }
    }

    public String getStartAt()
    {
        return startAt;
    }

    public void setEndAt(String endAt)
    {
        if (endAt != null)
        {
            this.endAt = endAt;
        }
        else
        {
            throw new ClassCastException("Club finishing time cannot be null");
        }
    }

    public String getEndAt()
    {
        return endAt;
    }

    public club(JSONObject club)
    {
        setTitle(club.optString("club"));
        setStartAt(club.optString("start"));
        setEndAt(club.optString("end"));
    }
}
