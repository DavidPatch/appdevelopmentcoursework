package com.example.patch.ascpaperless;

import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by David Marchant on 17/03/2016.
 *
 * This is the java for the child information dialog, which displays on clicking on a child's name
 * within the register fragment
 */
public class childInformation extends DialogFragment
{

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.child_information, container, false);

        final LinearLayout childInfoContainer = (LinearLayout)view.findViewById(R.id.container);

        // import all the various arguments and generate the relevant parts of the dialog
        String fullName = getArguments().getString("fullName");
        String dateOfBirth = getArguments().getString("dateOfBirth");
        String title = fullName +" - "+ dateOfBirth;
        getDialog().setTitle(title);

        displayChildInformation(childInfoContainer);

        Button information = (Button)view.findViewById(R.id.information);
        information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearContainer(childInfoContainer);
                displayChildInformation(childInfoContainer);
            }
        });

        Button point = (Button)view.findViewById(R.id.point);
        point.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                clearContainer(childInfoContainer);
                displayPointEntry(childInfoContainer);
            }
        });

        Button warning = (Button)view.findViewById(R.id.warning);
        warning.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                clearContainer(childInfoContainer);
                displayWarningEntry(childInfoContainer);
            }
        });

        Button accident = (Button)view.findViewById(R.id.accident);
        accident.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                clearContainer(childInfoContainer);
                displayAccidentEntry(childInfoContainer);
            }
        });

        Button incident = (Button)view.findViewById(R.id.incident);
        incident.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                clearContainer(childInfoContainer);
                displayIncidentEntry(childInfoContainer);
            }
        });

        Button confirm = (Button)view.findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                clearContainer(childInfoContainer);
                displayNoteEntry(childInfoContainer);
            }
        });

        return view;
    }

    public void clearContainer(LinearLayout container)
    {
        container.removeAllViews();
    }

    public void displayPointEntry(LinearLayout container)
    {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        TextView pointStaff = new TextView(getActivity());
        pointStaff.setText(getResources().getString(R.string.pointsStaff));
        pointStaff.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        layout.addView(pointStaff);

        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());
        ArrayList<staff> staffTeam = clubDay.getStaffTeam();
        ArrayList<String> spinnerArray = new ArrayList<>();
        for (int staffIndex = 0; staffIndex< staffTeam.size(); staffIndex++)
        {
            if (staffTeam.get(staffIndex).getWorking().equals("in") ||
                staffTeam.get(staffIndex).getWorking().equals("replacement"))
            {
                spinnerArray.add(staffTeam.get(staffIndex).getFullName());
            }
        }
        final Spinner staffSelector = new Spinner(getActivity());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        staffSelector.setAdapter(spinnerArrayAdapter);
        layout.addView(staffSelector);

        TextView pointReason = new TextView(getActivity());
        pointReason.setText(getResources().getString(R.string.pointsReason));
        pointReason.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        layout.addView(pointReason);

        final EditText pointReasonEntry = new EditText(getActivity());
        pointReasonEntry.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        pointReasonEntry.setInputType(InputType.TYPE_CLASS_TEXT);
        pointReasonEntry.setHorizontallyScrolling(false);
        pointReasonEntry.setLines(4);
        pointReasonEntry.setMinLines(1);
        pointReasonEntry.setMaxLines(10);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT);
        pointReasonEntry.setLayoutParams(layoutParams);
        layout.addView(pointReasonEntry);

        Button confirmButton = new Button(getActivity());
        confirmButton.setText(getResources().getString(R.string.pointsConfirm));
        confirmButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (pointReasonEntry.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(), "please enter a brief description", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getActivity(), "point awarded", Toast.LENGTH_SHORT).show();
                point point = new point(staffSelector.getSelectedItem().toString(), pointReasonEntry.getText().toString());
                ((MainActivity) getActivity()).addPoint(getArguments().getString("identifier"), point);

                // update the arguments allowing for up to date info
                Bundle args = getArguments();
                int pointCounter = getArguments().getInt("pointCounter");
                args.putString("point" + pointCounter, point.getPoint());
                pointCounter++;
                args.putInt("pointCounter", pointCounter);
                getArguments().putAll(args);
            }
        });
        layout.addView(confirmButton);

        container.addView(layout);
    }

    public void displayWarningEntry(LinearLayout container)
    {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        TextView warningStaff = new TextView(getActivity());
        warningStaff.setText(getResources().getString(R.string.warningsStaff));
        warningStaff.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        layout.addView(warningStaff);

        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());
        ArrayList<staff> staffTeam = clubDay.getStaffTeam();
        ArrayList<String> spinnerArray = new ArrayList<>();
        for (int staffIndex = 0; staffIndex< staffTeam.size(); staffIndex++)
        {
            if (staffTeam.get(staffIndex).getWorking().equals("in") ||
                staffTeam.get(staffIndex).getWorking().equals("replacement"))
            {
                spinnerArray.add(staffTeam.get(staffIndex).getFullName());
            }
        }
        final Spinner staffSelector = new Spinner(getActivity());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        staffSelector.setAdapter(spinnerArrayAdapter);
        layout.addView(staffSelector);

        TextView warningReason = new TextView(getActivity());
        warningReason.setText(getResources().getString(R.string.warningsReason));
        warningReason.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        layout.addView(warningReason);

        final EditText warningReasonEntry = new EditText(getActivity());
        warningReasonEntry.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        warningReasonEntry.setInputType(InputType.TYPE_CLASS_TEXT);
        warningReasonEntry.setHorizontallyScrolling(false);
        warningReasonEntry.setLines(4);
        warningReasonEntry.setMinLines(1);
        warningReasonEntry.setMaxLines(10);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT);
        warningReasonEntry.setLayoutParams(layoutParams);
        layout.addView(warningReasonEntry);

        Button confirmButton = new Button(getActivity());
        confirmButton.setText(getResources().getString(R.string.warningsConfirm));
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (warningReasonEntry.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(), "please enter a brief description", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getActivity(), "warning noted", Toast.LENGTH_SHORT).show();
                warning warning = new warning(staffSelector.getSelectedItem().toString(), warningReasonEntry.getText().toString());
                ((MainActivity) getActivity()).addWarning(getArguments().getString("identifier"), warning);

                // update the arguments allowing for up to date info
                Bundle args = getArguments();
                int warningCounter = getArguments().getInt("warningCounter");
                args.putString("warning" + warningCounter, warning.getWarning());
                warningCounter++;
                args.putInt("warningCounter", warningCounter);
                getArguments().putAll(args);
            }
        });
        layout.addView(confirmButton);

        container.addView(layout);
    }

    public void displayAccidentEntry(LinearLayout container)
    {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        TextView accidentStaff = new TextView(getActivity());
        accidentStaff.setText(getResources().getString(R.string.accidentsStaff));
        accidentStaff.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        layout.addView(accidentStaff);

        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());
        ArrayList<staff> staffTeam = clubDay.getStaffTeam();
        ArrayList<String> spinnerArray = new ArrayList<>();
        for (int staffIndex = 0; staffIndex< staffTeam.size(); staffIndex++)
        {
            if (staffTeam.get(staffIndex).getWorking().equals("in") ||
                staffTeam.get(staffIndex).getWorking().equals("replacement"))
            {
                spinnerArray.add(staffTeam.get(staffIndex).getFullName());
            }
        }
        final Spinner staffSelector = new Spinner(getActivity());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        staffSelector.setAdapter(spinnerArrayAdapter);
        layout.addView(staffSelector);

        TextView accidentReason = new TextView(getActivity());
        accidentReason.setText(getResources().getString(R.string.accidentsReason));
        accidentReason.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        layout.addView(accidentReason);

        final EditText accidentReasonEntry = new EditText(getActivity());
        accidentReasonEntry.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        accidentReasonEntry.setInputType(InputType.TYPE_CLASS_TEXT);
        accidentReasonEntry.setHorizontallyScrolling(false);
        accidentReasonEntry.setLines(4);
        accidentReasonEntry.setMinLines(1);
        accidentReasonEntry.setMaxLines(10);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT);
        accidentReasonEntry.setLayoutParams(layoutParams);
        layout.addView(accidentReasonEntry);

        Button confirmButton = new Button(getActivity());
        confirmButton.setText(getResources().getString(R.string.accidentsConfirm));
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (accidentReasonEntry.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(), "please enter a brief description", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getActivity(), "accident noted", Toast.LENGTH_SHORT).show();
                accident accident = new accident(staffSelector.getSelectedItem().toString(), accidentReasonEntry.getText().toString());
                ((MainActivity) getActivity()).addAccident(getArguments().getString("identifier"), accident);

                // update the arguments allowing for up to date info
                Bundle args = getArguments();
                int accidentCounter = getArguments().getInt("accidentCounter");
                args.putString("accident" + accidentCounter, accident.getAccident());
                accidentCounter++;
                args.putInt("accidentCounter", accidentCounter);
                getArguments().putAll(args);
            }
        });
        layout.addView(confirmButton);

        container.addView(layout);
    }

    public void displayIncidentEntry(LinearLayout container)
    {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        TextView incidentStaff = new TextView(getActivity());
        incidentStaff.setText(getResources().getString(R.string.incidentsStaff));
        incidentStaff.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        layout.addView(incidentStaff);

        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());
        ArrayList<staff> staffTeam = clubDay.getStaffTeam();
        ArrayList<String> spinnerArray = new ArrayList<>();
        for (int staffIndex = 0; staffIndex< staffTeam.size(); staffIndex++)
        {
            if (staffTeam.get(staffIndex).getWorking().equals("in") ||
                    staffTeam.get(staffIndex).getWorking().equals("replacement"))
            {
                spinnerArray.add(staffTeam.get(staffIndex).getFullName());
            }
        }
        final Spinner staffSelector = new Spinner(getActivity());
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        staffSelector.setAdapter(spinnerArrayAdapter);
        layout.addView(staffSelector);

        TextView incidentReason = new TextView(getActivity());
        incidentReason.setText(getResources().getString(R.string.incidentsReason));
        incidentReason.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        layout.addView(incidentReason);

        final EditText incidentReasonEntry = new EditText(getActivity());
        incidentReasonEntry.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        incidentReasonEntry.setInputType(InputType.TYPE_CLASS_TEXT);
        incidentReasonEntry.setHorizontallyScrolling(false);
        incidentReasonEntry.setLines(4);
        incidentReasonEntry.setMinLines(1);
        incidentReasonEntry.setMaxLines(10);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT);
        incidentReasonEntry.setLayoutParams(layoutParams);
        layout.addView(incidentReasonEntry);

        Button confirmButton = new Button(getActivity());
        confirmButton.setText(getResources().getString(R.string.incidentsConfirm));
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (incidentReasonEntry.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(), "please enter a brief description", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getActivity(), "incident noted", Toast.LENGTH_SHORT).show();
                incident incident = new incident(staffSelector.getSelectedItem().toString(), incidentReasonEntry.getText().toString());
                ((MainActivity) getActivity()).addIncident(getArguments().getString("identifier"), incident);

                // update the arguments allowing for up to date info
                Bundle args = getArguments();
                int incidentCounter = getArguments().getInt("incidentCounter");
                args.putString("incident" + incidentCounter, incident.getIncident());
                incidentCounter++;
                args.putInt("incidentCounter", incidentCounter);
                getArguments().putAll(args);
            }
        });
        layout.addView(confirmButton);

        container.addView(layout);
    }

    public void displayNoteEntry(LinearLayout container)
    {
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.VERTICAL);

        TextView noteTitle = new TextView(getActivity());
        noteTitle.setText(getResources().getString(R.string.noteTitle));
        noteTitle.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        layout.addView(noteTitle);

        final EditText noteEntry = new EditText(getActivity());
        noteEntry.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        noteEntry.setText(getArguments().getString("additional"));
        noteEntry.setInputType(InputType.TYPE_CLASS_TEXT);
        noteEntry.setHorizontallyScrolling(false);
        noteEntry.setLines(4);
        noteEntry.setMinLines(1);
        noteEntry.setMaxLines(10);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(500, LinearLayout.LayoutParams.WRAP_CONTENT);
        noteEntry.setLayoutParams(layoutParams);
        layout.addView(noteEntry);

        Button confirmButton = new Button(getActivity());
        confirmButton.setText(getResources().getString(R.string.incidentsConfirm));
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String additional = noteEntry.getText().toString();
                Toast.makeText(getActivity(), "comment noted", Toast.LENGTH_SHORT).show();
                ((MainActivity) getActivity()).updateAdditional(getArguments().getString("identifier"), additional);

                // update the arguments allowing for up to date info
                Bundle args = getArguments();
                args.putString("additional", additional);
                getArguments().putAll(args);
            }
        });
        layout.addView(confirmButton);

        container.addView(layout);
    }

    public void displayChildInformation(LinearLayout container)
    {
        TableLayout table = new TableLayout(getActivity());

        // generate the contact numbers
        TableRow contactsRow = new TableRow(getActivity());
        TextView contactTitle = new TextView(getActivity());
        contactTitle.setText(getResources().getString(R.string.childInformationContacts));
        contactTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        contactsRow.addView(contactTitle);
        LinearLayout contactContainer = new LinearLayout(getActivity());
        contactContainer.setOrientation(LinearLayout.VERTICAL);
        int contactCounter = getArguments().getInt("contactCounter");
        for (int contactsIndex = 0; contactsIndex<contactCounter; contactsIndex++)
        {
            TextView contact = new TextView(getActivity());
            contact.setText(getArguments().getString("contact"+ contactsIndex));
            contact.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
            contactContainer.addView(contact);
        }
        contactsRow.addView(contactContainer);
        table.addView(contactsRow);

        // generate medical
        TableRow medicalRow = new TableRow(getActivity());
        TextView medicalTitle = new TextView(getActivity());
        medicalTitle.setText(getResources().getString(R.string.childInformationMedical));
        medicalTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        medicalRow.addView(medicalTitle);
        int medicalCounter = getArguments().getInt("medicalCounter");
        String medicalBuffer = "";
        TextView medical = new TextView(getActivity());
        if (getArguments().getInt("medicalCounter") != 0)
        {
            for (int medicalIndex=0; medicalIndex<medicalCounter; medicalIndex++)
            {
                if (medicalBuffer.equals(""))
                {
                    medicalBuffer = getArguments().getString("medical"+ medicalIndex);
                }
                else
                {
                    medicalBuffer = medicalBuffer +", "+ getArguments().getString("medical"+ medicalIndex);
                }
            }
        }
        if (medicalBuffer.equals(""))
        {
            medicalBuffer = "None";
        }
        medical.setText(medicalBuffer);
        medical.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        medicalRow.addView(medical);
        table.addView(medicalRow);

        // generate allergies
        TableRow allergyRow = new TableRow(getActivity());
        TextView allergyTitle = new TextView(getActivity());
        allergyTitle.setText(getResources().getString(R.string.childInformationAllergies));
        allergyTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        allergyRow.addView(allergyTitle);
        String allergyBuffer = "";
        TextView allergy = new TextView(getActivity());
        int allergyCounter = getArguments().getInt("allergiesCounter");
        if (allergyCounter != 0)
        {
            for (int allergyIndex=0; allergyIndex<allergyCounter; allergyIndex++)
            {
                if (allergyBuffer.equals(""))
                {
                    allergyBuffer = getArguments().getString("allergy"+ allergyIndex);
                }
                else
                {
                    allergyBuffer = allergyBuffer +", "+ getArguments().getString("allergy"+ allergyIndex);
                }
            }
        }
        if (allergyBuffer.equals(""))
        {
            allergyBuffer = "None";
        }
        allergy.setText(allergyBuffer);
        allergy.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        allergyRow.addView(allergy);
        table.addView(allergyRow);

        // generate clubs
        TableRow clubsRow = new TableRow(getActivity());
        TextView clubsTitle = new TextView(getActivity());
        clubsTitle.setText(getResources().getString(R.string.childInformationClubs));
        clubsTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        clubsRow.addView(clubsTitle);
        String clubsBuffer = "";
        TextView clubs = new TextView(getActivity());
        int clubCounter = getArguments().getInt("clubCounter");
        if (clubCounter != 0)
        {
            for (int clubIndex=0; clubIndex<clubCounter; clubIndex++)
            {
                if (clubsBuffer.equals(""))
                {
                    clubsBuffer = getArguments().getString("club"+ clubIndex);
                }
                else
                {
                    clubsBuffer = clubsBuffer +", "+ getArguments().getString("club"+ clubIndex);
                }
            }
        }
        if (clubsBuffer.equals(""))
        {
            clubsBuffer = "None";
        }
        clubs.setText(clubsBuffer);
        clubs.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        clubsRow.addView(clubs);
        table.addView(clubsRow);

        // generate points
        TableRow pointsRow = new TableRow(getActivity());
        TextView pointsTitle = new TextView(getActivity());
        pointsTitle.setText(getResources().getString(R.string.childInformationPoints));
        pointsTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        pointsRow.addView(pointsTitle);
        String pointsBuffer = "";
        TextView points = new TextView(getActivity());
        int pointCounter = getArguments().getInt("pointCounter");
        if (pointCounter != 0)
        {
            for (int pointIndex=0; pointIndex<pointCounter; pointIndex++)
            {
                if (pointsBuffer.equals(""))
                {
                    pointsBuffer = getArguments().getString("point"+ pointIndex);
                }
                else
                {
                    pointsBuffer = pointsBuffer +",\n"+ getArguments().getString("point"+ pointIndex);
                }
            }
        }
        if (pointsBuffer.equals(""))
        {
            pointsBuffer = "None";
        }
        points.setText(pointsBuffer);
        points.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        points.setHorizontallyScrolling(false);
        points.setMinLines(1);
        TableRow.LayoutParams pointsLayoutParams = new TableRow.LayoutParams(400, TableRow.LayoutParams.WRAP_CONTENT);
        points.setLayoutParams(pointsLayoutParams);
        pointsRow.addView(points);
        table.addView(pointsRow);

        // generate warnings
        TableRow warningsRow = new TableRow(getActivity());
        TextView warningsTitle = new TextView(getActivity());
        warningsTitle.setText(getResources().getString(R.string.childInformationWarnings));
        warningsTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        warningsRow.addView(warningsTitle);
        String warningsBuffer = "";
        TextView warnings = new TextView(getActivity());
        int warningCounter = getArguments().getInt("warningCounter");
        if (warningCounter != 0)
        {
            for (int warningIndex=0; warningIndex<warningCounter; warningIndex++)
            {
                if (warningsBuffer.equals(""))
                {
                    warningsBuffer = getArguments().getString("warning"+ warningIndex);
                }
                else
                {
                    warningsBuffer = warningsBuffer +",\n"+ getArguments().getString("warning"+ warningIndex);
                }
            }
        }
        if (warningsBuffer.equals(""))
        {
            warningsBuffer = "None";
        }
        warnings.setText(warningsBuffer);
        warnings.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        warnings.setHorizontallyScrolling(false);
        warnings.setMinLines(1);
        TableRow.LayoutParams warningsLayoutParams = new TableRow.LayoutParams(400, TableRow.LayoutParams.WRAP_CONTENT);
        warnings.setLayoutParams(warningsLayoutParams);
        warningsRow.addView(warnings);
        table.addView(warningsRow);

        // generate accidents
        TableRow accidentsRow = new TableRow(getActivity());
        TextView accidentsTitle = new TextView(getActivity());
        accidentsTitle.setText(getResources().getString(R.string.childInformationAccidents));
        accidentsTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        accidentsRow.addView(accidentsTitle);
        String accidentsBuffer = "";
        TextView accidents = new TextView(getActivity());
        int accidentCounter = getArguments().getInt("accidentCounter");
        if (accidentCounter != 0)
        {
            for (int accidentsIndex=0; accidentsIndex<accidentCounter; accidentsIndex++)
            {
                if (accidentsBuffer.equals(""))
                {
                    accidentsBuffer = getArguments().getString("accident"+ accidentsIndex);
                }
                else
                {
                    accidentsBuffer = accidentsBuffer +", "+ getArguments().getString("accident"+ accidentsIndex);
                }
            }
        }
        if (accidentsBuffer.equals(""))
        {
            accidentsBuffer = "None";
        }
        accidents.setText(accidentsBuffer);
        accidents.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        accidents.setHorizontallyScrolling(false);
        accidents.setMinLines(1);
        TableRow.LayoutParams accidentsLayoutParams = new TableRow.LayoutParams(400, TableRow.LayoutParams.WRAP_CONTENT);
        accidents.setLayoutParams(accidentsLayoutParams);
        accidentsRow.addView(accidents);
        table.addView(accidentsRow);

        // generate incidents
        TableRow incidentsRow = new TableRow(getActivity());
        TextView incidentsTitle = new TextView(getActivity());
        incidentsTitle.setText(getResources().getString(R.string.childInformationIncidents));
        incidentsTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        incidentsRow.addView(incidentsTitle);
        String incidentsBuffer = "";
        TextView incidents = new TextView(getActivity());
        int incidentCounter = getArguments().getInt("incidentCounter");
        if (incidentCounter != 0)
        {
            for (int incidentIndex=0; incidentIndex<incidentCounter; incidentIndex++)
            {
                if (incidentsBuffer.equals(""))
                {
                    incidentsBuffer = getArguments().getString("incident"+ incidentIndex);
                }
                else
                {
                    incidentsBuffer = incidentsBuffer +", "+ getArguments().getString("incident"+ incidentIndex);
                }
            }
        }
        if (incidentsBuffer.equals(""))
        {
            incidentsBuffer = "None";
        }
        incidents.setText(incidentsBuffer);
        incidents.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        incidents.setHorizontallyScrolling(false);
        incidents.setMinLines(1);
        TableRow.LayoutParams incidentsLayoutParams = new TableRow.LayoutParams(400, TableRow.LayoutParams.WRAP_CONTENT);
        incidents.setLayoutParams(incidentsLayoutParams);
        incidentsRow.addView(incidents);
        table.addView(incidentsRow);

        // generate additional
        TableRow additionalRow = new TableRow(getActivity());
        TextView additionalTitle = new TextView(getActivity());
        additionalTitle.setText(getResources().getString(R.string.childInformationAdditional));
        additionalTitle.setTextSize(getResources().getDimension(R.dimen.childInformationTitleFontSize));
        additionalRow.addView(additionalTitle);
        TextView additional = new TextView(getActivity());
        String additionalBuffer = getArguments().getString("additional");
        additional.setText(additionalBuffer);
        additional.setTextSize(getResources().getDimension(R.dimen.childInformationFontSize));
        additionalRow.addView(additional);
        table.addView(additionalRow);

        container.addView(table);

        // display signature
        String signaturePath = getArguments().getString("signature");
        if (!signaturePath.equals("No"))
        {
            signaturePath = "/storage/emulated/0/saved_signature/"+ signaturePath;
            ImageView signature = new ImageView(getActivity());
            Bitmap image = BitmapFactory.decodeFile(signaturePath);
            signature.setImageBitmap(image);
            container.addView(signature);
        }
    }
}
