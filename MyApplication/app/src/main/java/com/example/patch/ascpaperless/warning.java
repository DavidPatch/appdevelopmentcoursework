package com.example.patch.ascpaperless;

/**
 * Created by David Marchant on 23/03/2016.
 *
 * This is the class description for the warning class
 */
public class warning
{
    private String staff;
    private String offence;

    public void setStaff(String staff)
    {
        if (staff != null)
        {
            this.staff = staff;
        }
        else
        {
            throw new ClassCastException("Staff name cannot cannot be null");
        }
    }

    public String getStaff()
    {
        return staff;
    }

    public void setOffence(String offence)
    {
        this.offence = offence;
    }

    public String getOffence()
    {
        return offence;
    }

    public String getWarning()
    {
        return getStaff() +" - "+ getOffence();
    }

    public warning(String staff, String description)
    {
        setStaff(staff);
        setOffence(description);
    }
}
