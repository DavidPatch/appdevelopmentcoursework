package com.example.patch.ascpaperless;

import android.app.Activity;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by David Marchant on 08/03/2016.
 *
 * This is the java for the register fragment. it displays all children who are in today along with
 * all the important information about them. The information is colour coded along with the alerts
 * for ease of reading
 */
public class registerFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    public void fillRegister(final ArrayList<child> children)
    {
        TableLayout register = (TableLayout)getView().findViewById(R.id.registerTable);

        // clear the register before populating. Make sure to keep the top line though
        while (register.getChildCount() > 1)
        {
            register.removeView(register.getChildAt(register.getChildCount() - 1));
        }

        int childCounter = 0;
        for (int childIndex=0; childIndex<children.size(); childIndex++)
        {
            final child child = children.get(childIndex);
            if (child.getBooked().equals("regular") || child.getBooked().equals("extra"))
            {
                childCounter++;

                // setup a new table row
                TableRow registerEntry = new TableRow(getActivity());

                String rowID = child.getIdentifier();
                registerEntry.setId(childIndex);
                registerEntry.setContentDescription(rowID);

                // add a line between different rows
                View divider = new View(getActivity());
                divider.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
                divider.setBackgroundColor(getResources().getColor(R.color.divider));
                register.addView(divider);

                // add a child counter
                TextView count = new TextView(getActivity());
                String countBuffer = Integer.toString(childCounter);
                count.setText(countBuffer);
                count.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                registerEntry.addView(count);

                // put in a divider to help separate columns
                View verticalDivider0 = new View(getActivity());
                verticalDivider0.setLayoutParams(new TableRow.LayoutParams(1, TableRow.LayoutParams.FILL_PARENT));
                verticalDivider0.setBackgroundColor(getResources().getColor(R.color.divider));
                registerEntry.addView(verticalDivider0);

                // add the checkbox for arrival
                final ImageView checkbox = new ImageView(getActivity());
                if (!child.getInTheClub()) {
                    checkbox.setImageResource(R.mipmap.empty);
                    checkbox.setTag("empty");
                } else {
                    checkbox.setImageResource(R.mipmap.tick);
                    checkbox.setTag("tick");
                }
                checkbox.setContentDescription(child.getIdentifier());
                checkbox.setLayoutParams(new TableRow.LayoutParams(25, 25));
                checkbox.setClickable(true);

                registerEntry.addView(checkbox);

                // put in a divider to help separate columns
                View verticalDivider = new View(getActivity());
                verticalDivider.setLayoutParams(new TableRow.LayoutParams(1, TableRow.LayoutParams.FILL_PARENT));
                verticalDivider.setBackgroundColor(getResources().getColor(R.color.divider));
                registerEntry.addView(verticalDivider);

                // add the child's primary year
                TextView primary = new TextView(getActivity());
                primary.setText(child.getPrimary());
                primary.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                registerEntry.addView(primary);

                // put in a divider to help separate columns
                View verticalDivider1 = new View(getActivity());
                verticalDivider1.setLayoutParams(new TableRow.LayoutParams(1, TableRow.LayoutParams.FILL_PARENT));
                verticalDivider1.setBackgroundColor(getResources().getColor(R.color.divider));
                registerEntry.addView(verticalDivider1);

                // add the child's name
                final TextView name = new TextView(getActivity());
                name.setText(child.getFullName());
                name.setTextSize(getResources().getDimension(R.dimen.NameFontSize));
                name.setContentDescription(child.getIdentifier());
                name.setClickable(true);
                registerEntry.addView(name);

                // put in a divider to help separate columns
                View verticalDivider2 = new View(getActivity());
                verticalDivider2.setLayoutParams(new TableRow.LayoutParams(1, TableRow.LayoutParams.FILL_PARENT));
                verticalDivider2.setBackgroundColor(getResources().getColor(R.color.divider));
                registerEntry.addView(verticalDivider2);

                // setup the final box which lists any and all other info
                LinearLayout additional = new LinearLayout(getActivity());
                TableRow.LayoutParams additionalLayout = new TableRow.LayoutParams();
                additionalLayout.height = TableRow.LayoutParams.MATCH_PARENT;
                additional.setLayoutParams(additionalLayout);
                additional.setOrientation(LinearLayout.HORIZONTAL);

                // notify if signed out
                if (!child.getSignedOut().equals("No"))
                {
                    TextView signedOut = new TextView(getActivity());
                    signedOut.setText(" Signed Out ");
                    signedOut.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                    signedOut.setBackgroundColor(getResources().getColor(R.color.signedOut));
                    additional.addView(signedOut);
                }

                // add any clubs the child attends
                if (child.getClubs().size() >= 1) {
                    ArrayList<String> clubs = child.getClubs();
                    for (int clubsIndex = 0; clubsIndex < clubs.size(); clubsIndex++) {
                        TextView club = new TextView(getActivity());
                        club.setText(" " + clubs.get(clubsIndex) + " ");
                        club.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                        club.setBackgroundColor(getResources().getColor(R.color.clubs));
                        additional.addView(club);
                    }
                }

                // add any medical conditions
                if (child.getMedical().size() >= 1) {
                    ArrayList<String> medicalNeeds = child.getMedical();
                    for (int medicalIndex = 0; medicalIndex < medicalNeeds.size(); medicalIndex++) {
                        TextView medical = new TextView(getActivity());
                        medical.setText(" " + medicalNeeds.get(medicalIndex) + " ");
                        medical.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                        medical.setBackgroundColor(getResources().getColor(R.color.medical));
                        additional.addView(medical);
                    }
                }

                // add any allergies
                if (child.getAllergies().size() >= 1) {
                    ArrayList<String> allAllergies = child.getAllergies();
                    for (int allergiesIndex = 0; allergiesIndex < allAllergies.size(); allergiesIndex++) {
                        TextView allergies = new TextView(getActivity());
                        allergies.setText(" " + allAllergies.get(allergiesIndex) + " ");
                        allergies.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                        allergies.setBackgroundColor(getResources().getColor(R.color.allergies));
                        additional.addView(allergies);
                    }
                }

                // add any additional needs
                if (!child.getAdditional().equals("None")) {
                    TextView additionalInfo = new TextView(getActivity());
                    additionalInfo.setText(" " + child.getAdditional() + " ");
                    additionalInfo.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                    additionalInfo.setBackgroundColor(getResources().getColor(R.color.additional));
                    additional.addView(additionalInfo);
                }
                registerEntry.addView(additional);

                // add a point counter
                if (child.getPoints().size()>0)
                {
                    int pointCounter = 0;
                    for (int pointIndex = 0; pointIndex<child.getPoints().size(); pointIndex++)
                    {
                        pointCounter++;
                    }
                    TextView points = new TextView(getActivity());
                    if (pointCounter > 1)
                    {
                        points.setText(" "+ pointCounter +" Points ");
                    }
                    else
                    {
                        points.setText(" "+ pointCounter +" Point ");
                    }
                    points.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                    points.setBackgroundColor(getResources().getColor(R.color.points));
                    additional.addView(points);
                }

                // add a warning counter
                if (child.getWarnings().size()>0)
                {
                    int warningCounter = 0;
                    for (int warningIndex = 0; warningIndex<child.getWarnings().size(); warningIndex++)
                    {
                        warningCounter++;
                    }
                    TextView warnings = new TextView(getActivity());
                    if (warningCounter > 1)
                    {
                        warnings.setText(" "+ warningCounter +" Warnings ");
                    }
                    else
                    {
                        warnings.setText(" "+ warningCounter +" Warning ");
                    }
                    warnings.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                    warnings.setBackgroundColor(getResources().getColor(R.color.warnings));
                    additional.addView(warnings);
                }

                // add an accident counter
                if (child.getAccidents().size()>0)
                {
                    int accidentCounter = 0;
                    for (int accidentIndex = 0; accidentIndex<child.getAccidents().size(); accidentIndex++)
                    {
                        accidentCounter++;
                    }
                    TextView accidents = new TextView(getActivity());
                    if (accidentCounter > 1)
                    {
                        accidents.setText(" "+ accidentCounter +" Accidents ");
                    }
                    else
                    {
                        accidents.setText(" "+ accidentCounter +" Accident ");
                    }
                    accidents.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                    accidents.setBackgroundColor(getResources().getColor(R.color.accidents));
                    additional.addView(accidents);
                }

                // add an incident counter
                if (child.getIncidents().size()>0)
                {
                    int incidentCounter = 0;
                    for (int incidentIndex = 0; incidentIndex<child.getIncidents().size(); incidentIndex++)
                    {
                        incidentCounter++;
                    }
                    TextView incidents = new TextView(getActivity());
                    if (incidentCounter > 1)
                    {
                        incidents.setText(" "+ incidentCounter +" Incidents ");
                    }
                    else
                    {
                        incidents.setText(" "+ incidentCounter +" Incident ");
                    }
                    incidents.setTextSize(getResources().getDimension(R.dimen.otherFontSize));
                    incidents.setBackgroundColor(getResources().getColor(R.color.incidents));
                    additional.addView(incidents);
                }

                // add a click listener for the name
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((MainActivity) getActivity()).displayChildInfo(child);
                    }
                });

                // add a click listener for the checkbox
                if (child.getSignedOut().equals("No"))
                {
                    checkbox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (checkbox.getTag().equals("empty")) {
                                ((MainActivity) getActivity()).updateInTheClub(checkbox.getContentDescription().toString(), true);
                                checkbox.setImageResource(R.mipmap.tick);
                                checkbox.setTag("tick");
                            } else {
                                ((MainActivity) getActivity()).updateInTheClub(checkbox.getContentDescription().toString(), false);
                                checkbox.setImageResource(R.mipmap.empty);
                                checkbox.setTag("empty");
                            }
                        }
                    });
                }


                // add the row to the register
                register.addView(registerEntry);
            }
        }
    }
}
