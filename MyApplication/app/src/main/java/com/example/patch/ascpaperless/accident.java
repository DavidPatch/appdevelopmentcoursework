package com.example.patch.ascpaperless;

/**
 * Created by David Marchant on 23/03/2016.
 *
 * This is the class description for an accident
 */
public class accident
{
    private String staff;
    private String description;

    public void setStaff(String staff)
    {
        if (staff != null)
        {
            this.staff = staff;
        }
        else
        {
            throw new ClassCastException("Staff name cannot cannot be null");
        }
    }

    public String getStaff()
    {
        return staff;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public String getAccident()
    {
        return getStaff() +" - "+ getDescription();
    }

    public accident(String staff, String description)
    {
        setStaff(staff);
        setDescription(description);
    }
}