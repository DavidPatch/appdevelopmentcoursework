package com.example.patch.ascpaperless;

import android.app.Application;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by David Marchant on 15/03/2016.
 *
 * This is the class description for the clubDay. This stores is the frame for all the JSON
 * information and is stored as Application context, meaning it should be accessible from anywhere
 */
public class clubDay extends Application
{
    private ArrayList<child> register  = new ArrayList<>();
    private ArrayList<staff> staffTeam  = new ArrayList<>();
    private ArrayList<club> clubs = new ArrayList<>();

    public ArrayList<child> getRegister()
    {
        return register;
    }

    public ArrayList<staff> getStaffTeam()
    {
        return staffTeam;
    }

    public ArrayList<club> getClubs()
    {
        return clubs;
    }

    public void addChild(JSONObject childToAdd)
    {
        child newChild = new child(childToAdd);
        register.add(newChild);
    }

    public void addClub(JSONObject clubToAdd)
    {
        club newClub = new club(clubToAdd);
        clubs.add(newClub);
    }

    public void addStaff(JSONObject staffToAdd)
    {
        staff newStaff = new staff(staffToAdd);
        staffTeam.add(newStaff);
    }

    public void sortRegisterByPrimary()
    {
        Collections.sort(register, new Comparator<child>() {
            @Override
            public int compare(child child1, child child2) {
                return child1.getSortableByPrimary().compareTo(child2.getSortableByPrimary());
            }
        });
    }

    public void sortRegisterBySurname()
    {
        Collections.sort(register, new Comparator<child>() {
            @Override
            public int compare(child child1, child child2) {
                return child1.getSurname().compareTo(child2.getSurname());
            }
        });
    }
}
