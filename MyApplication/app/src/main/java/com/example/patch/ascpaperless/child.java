package com.example.patch.ascpaperless;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by David Marchant on 15/03/2016.
 *
 * This is the class description of the child class. It contains all the information used to generate
 * the register and alerts and is one of the things generated from the initial JSON
 */
public class child
{
    private String firstName;
    private String surname;
    private String dateOfBirth;
    private String primary;
    private String booked;
    private String additional;
    private String signedOut;
    private Boolean inTheClub;
    private ArrayList<String> allergies = new ArrayList<>();
    private ArrayList<String> medical = new ArrayList<>();
    private ArrayList<String> clubs = new ArrayList<>();
    private ArrayList<contact> contacts = new ArrayList<>();
    private ArrayList<warning> warnings = new ArrayList<>();
    private ArrayList<point> points = new ArrayList<>();
    private ArrayList<accident> accidents = new ArrayList<>();
    private ArrayList<incident> incidents = new ArrayList<>();

    public void setFirstName(String firstName)
    {
        if (firstName != null)
        {
            this.firstName = firstName;
        }
        else
        {
            throw new ClassCastException("Child first name cannot be null");
        }
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setSurname(String surname)
    {
        if (surname != null)
        {
            this.surname = surname;
        }
        else
        {
            throw new ClassCastException("Child surname cannot be null");
        }
    }

    public String getSurname()
    {
        return surname;
    }

    public void signOut(String signature)
    {
        signedOut = signature;
        inTheClub = false;
    }

    public void setSignedOut(String sign)
    {
        signedOut = sign;
    }

    public String getSignedOut()
    {
        return signedOut;
    }

    public void setDateOfBirth(String dateOfBirth)
    {
        if (dateOfBirth != null)
        {
            this.dateOfBirth = dateOfBirth;
        }
        else
        {
            throw new ClassCastException("Child date of birth cannot be null");
        }
    }

    public String getDateOfBirth()
    {
        return dateOfBirth;
    }

    public void setPrimary(String primary)
    {
        if (primary.equals("P1") ||
            primary.equals("P2") ||
            primary.equals("P3") ||
            primary.equals("P4") ||
            primary.equals("P5") ||
            primary.equals("P6") ||
            primary.equals("P7"))
        {
            this.primary = primary;
        }
        else
        {
            this.primary = "P?";
        }
    }

    public String getPrimary()
    {
        return primary;
    }

    public void setBooked(String booked)
    {
        if (booked.equals("regular") || booked.equals("extra") || booked.equals("absent"))
        {
            this.booked = booked;
        }
        else
        {
            throw new ClassCastException("Child booking can only be 'regular', 'extra' or 'absent'");
        }
    }

    public String getBooked()
    {
        return booked;
    }

    public void setAdditional(String additional)
    {
        if (additional.equals("") ||
            additional.equals("none") ||
            additional.equals("N") ||
            additional.equals("None ") ||
            additional.equals("Non") ||
            additional.equals("No") ||
            additional.equals("no"))
        {
            additional = "None";
        }
        this.additional = additional;
    }

    public String getAdditional()
    {
        return additional;
    }

    public void setInTheClub(Boolean inTheClub)
    {
        this.inTheClub = inTheClub;
    }

    public Boolean getInTheClub()
    {
        return inTheClub;
    }

    public void addToAllergies(String allergy)
    {
        this.allergies.add(allergy);
    }

    public ArrayList<String> getAllergies()
    {
        return allergies;
    }

    public void addToWarnings(warning warning)
    {
        this.warnings.add(warning);
    }

    public ArrayList<warning> getWarnings()
    {
        return warnings;
    }

    public void addToPoints(point point)
    {
        this.points.add(point);
    }

    public ArrayList<point> getPoints()
    {
        return points;
    }

    public void addToAccidents(accident accident)
    {
        this.accidents.add(accident);
    }

    public ArrayList<accident> getAccidents()
    {
        return accidents;
    }

    public void addToIncidents(incident incident)
    {
        this.incidents.add(incident);
    }

    public ArrayList<incident> getIncidents()
    {
        return incidents;
    }

    public void addToMedical(String medical)
    {
        this.medical.add(medical);
    }

    public ArrayList<String> getMedical()
    {
        return medical;
    }

    // TODO make sure that set clubs are valid clubs?
    public void addToClubs(String club)
    {
        this.clubs.add(club);
    }

    public ArrayList<String> getClubs()
    {
        return clubs;
    }

    public void addToContacts(contact contact)
    {
        this.contacts.add(contact);
    }

    public ArrayList<contact> getContacts()
    {
        return contacts;
    }

    public String getFullName()
    {
        return getFirstName() +" "+ getSurname();
    }

    public String getSortableByPrimary()
    {
        return getPrimary() +" "+ getSurname();
    }

    public String getIdentifier()
    {
        return getFullName() +" "+ getDateOfBirth();
    }

    public child(JSONObject child)
    {
        try
        {
            setFirstName(child.optString("firstName"));
            setSurname(child.optString("surname"));
            setDateOfBirth(child.optString("dateOfBirth"));
            setPrimary(child.optString("primary"));
            setBooked(child.optString("booked"));
            setAdditional(child.optString("additional"));
            setInTheClub(false);
            setSignedOut("No");
            if (!child.optString("allergies").equals("[]"))
            {
                JSONArray allAllergies = child.getJSONArray("allergies");
                for (int allergiesIndex = 0; allergiesIndex < allAllergies.length(); allergiesIndex++)
                {
                    addToAllergies(allAllergies.getString(allergiesIndex));
                }
            }
            if (!child.optString("medical").equals("[]"))
            {
                JSONArray allMedical = child.getJSONArray("medical");
                for (int medicalIndex = 0; medicalIndex < allMedical.length(); medicalIndex++)
                {
                    addToMedical(allMedical.getString(medicalIndex));
                }
            }
            if (!child.optString("clubs").equals("[]"))
            {
                JSONArray allClubs = child.getJSONArray("clubs");
                for (int clubsIndex = 0; clubsIndex < allClubs.length(); clubsIndex++)
                {
                    addToClubs(allClubs.getString(clubsIndex));
                }
            }
            if (!child.optString("phoneNumbers").equals("[]"))
            {
                JSONArray allContacts = child.getJSONArray("phoneNumbers");
                for (int contactsIndex = 0; contactsIndex < allContacts.length(); contactsIndex++)
                {
                    contact contact = new contact(allContacts.getJSONObject(contactsIndex));
                    addToContacts(contact);
                }
            }
        }
        catch (JSONException error)
        {
            error.printStackTrace();
        }
    }
}
