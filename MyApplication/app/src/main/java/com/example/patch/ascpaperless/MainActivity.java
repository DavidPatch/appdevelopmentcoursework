package com.example.patch.ascpaperless;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by David Marchant on 29/03/2016.
 *
 * This is our main activity. It imports a JSON file and converts it into usable classes. From here
 * it constructs registers and alerts
 */

public class MainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // add the alert and register fragment programmatically. make sure to take account of orientation

        if (clubDay.getRegister().isEmpty() &&
            clubDay.getClubs().isEmpty() &&
            clubDay.getStaffTeam().isEmpty())
        {
            try
            {
                // import the json file
                String allJSON = loadJSONFromAsset("asc.json");
                JSONObject json = new JSONObject(allJSON);

                // add all the children to the register
                JSONArray children = json.optJSONArray("children");
                for (int childIndex = 0; childIndex < children.length(); childIndex++)
                {
                    JSONObject child = children.getJSONObject(childIndex);
                    clubDay.addChild(child);
                }

                // add all the staff
                JSONArray staff = json.optJSONArray("staff");
                for (int staffIndex = 0; staffIndex < staff.length(); staffIndex++)
                {
                    JSONObject staffMember = staff.getJSONObject(staffIndex);
                    clubDay.addStaff(staffMember);
                }

                // add all the clubs
                JSONArray clubs = json.optJSONArray("clubs");
                for (int clubIndex = 0; clubIndex < clubs.length(); clubIndex++)
                {
                    JSONObject club = clubs.getJSONObject(clubIndex);
                    clubDay.addClub(club);
                }
            }
            catch (JSONException error)
            {
                error.printStackTrace();
            }
        }
        // sort the register into alphabetical order by primary
        clubDay.sortRegisterByPrimary();

        // pass the clubDay register to the alert and register fragments if the app is landscape
        if (findViewById(R.id.mainActivityLandscape) != null)
        {
            refreshRegister();

            //pass the clubDay register to the register fragment
            registerFragment registerFragment = (registerFragment) getFragmentManager().findFragmentById(R.id.register);
            ArrayList<child> children = clubDay.getRegister();
            registerFragment.fillRegister(children);

            alertFragment alertFragment = (alertFragment) getFragmentManager().findFragmentById(R.id.alerts);
            ArrayList<staff> staffTeam = clubDay.getStaffTeam();
            ArrayList<club> clubs = clubDay.getClubs();
            if (alertFragment != null)
            {
                alertFragment.generateAlerts(children, staffTeam, clubs);
            }
        }
        else
        {
            //pass the clubDay register to the parent register fragment
            parentRegisterFragment parentRegisterFragment = (parentRegisterFragment) getFragmentManager().findFragmentById(R.id.parentRegister);
            clubDay.sortRegisterBySurname();
            ArrayList<child> children = clubDay.getRegister();
            parentRegisterFragment.fillRegister(children);
            clubDay.sortRegisterByPrimary();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void refreshRegister()
    {
        registerFragment registerFragment = (registerFragment) getFragmentManager().findFragmentById(R.id.register);
        clubDay clubDay = ((clubDay)getApplicationContext());
        ArrayList<child> children = clubDay.getRegister();
        registerFragment.fillRegister(children);
    }

    public void refreshAlerts()
    {
        alertFragment alertFragment = (alertFragment) getFragmentManager().findFragmentById(R.id.alerts);
        clubDay clubDay = ((clubDay)getApplicationContext());
        ArrayList<child> children = clubDay.getRegister();
        ArrayList<staff> staffTeam = clubDay.getStaffTeam();
        ArrayList<club> clubs = clubDay.getClubs();
        alertFragment.generateAlerts(children, staffTeam, clubs);
    }


    public void updateInTheClub(String childID, Boolean inTheClub)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size() ;childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).setInTheClub(inTheClub);
            }
        }
    }

    public void cancellation(String childID)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size(); childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).setInTheClub(false);
                clubDay.getRegister().get(childIndex).setBooked("absent");
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void extraBooking(String childID)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size(); childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).setInTheClub(false);
                clubDay.getRegister().get(childIndex).setBooked("extra");
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void extraStaff(String staffID)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int staffIndex = 0; staffIndex<clubDay.getStaffTeam().size(); staffIndex++)
        {
            if (clubDay.getStaffTeam().get(staffIndex).getFullName().equals(staffID))
            {
                clubDay.getStaffTeam().get(staffIndex).setWorking("replacement");
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void staffAbsence(String staffID)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int staffIndex = 0; staffIndex<clubDay.getStaffTeam().size(); staffIndex++)
        {
            if (clubDay.getStaffTeam().get(staffIndex).getFullName().equals(staffID))
            {
                clubDay.getStaffTeam().get(staffIndex).setWorking("ill");
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void addPoint(String childID, point point)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size(); childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).addToPoints(point);
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void addWarning(String childID, warning warning)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size(); childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).addToWarnings(warning);
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void addAccident(String childID, accident accident)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size(); childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).addToAccidents(accident);
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void addIncident(String childID, incident incident)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size(); childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).addToIncidents(incident);
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public void updateAdditional(String childID, String additional)
    {
        // Initialise clubDay
        clubDay clubDay = ((clubDay)getApplicationContext());

        for (int childIndex = 0; childIndex<clubDay.getRegister().size(); childIndex++)
        {
            if (clubDay.getRegister().get(childIndex).getIdentifier().equals(childID))
            {
                clubDay.getRegister().get(childIndex).setAdditional(additional);
                refreshRegister();
                refreshAlerts();
                return;
            }
        }
    }

    public String loadJSONFromAsset(String toRead)
    {
        String json;
        try {
            InputStream inputStream = getAssets().open(toRead);
            //open("asc.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException error) {
            error.printStackTrace();
            return null;
        }
        return json;
    }

    public void displayChildInfo(child child)
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        childInformation dialog = new childInformation();

        Bundle args = generateArguments(child);
        dialog.setArguments(args);

        dialog.show(ft, "dialog");
    }

    public void displayExtraBooking()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        extraBooking dialog = new extraBooking();
        dialog.show(ft, "extraBooking");
    }

    public void displayCancellation()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        cancellation dialog = new cancellation();
        dialog.show(ft, "cancellation");
    }

    public void displayExtraStaff()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        extraStaff dialog = new extraStaff();
        dialog.show(ft, "extraStaff");
    }

    public void displayStaffAbsence()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        staffAbsence dialog = new staffAbsence();
        dialog.show(ft, "staffAbsence");
    }

    public Bundle generateArguments(child child)
    {
        // generate all the arguments for the dialog
        Bundle args = new Bundle();
        args.putString("fullName", child.getFullName());
        args.putString("identifier", child.getIdentifier());
        args.putString("additional", child.getAdditional());
        args.putString("signature", child.getSignedOut());
        int pointCounter = 0;
        for (int pointIndex = 0; pointIndex<child.getPoints().size(); pointIndex++)
        {
            args.putString("point"+ pointIndex, child.getPoints().get(pointIndex).getPoint());
            pointCounter++;
        }
        args.putInt("pointCounter", pointCounter);
        int warningCounter = 0;
        for (int warningIndex = 0; warningIndex<child.getWarnings().size(); warningIndex++)
        {
            args.putString("warning"+ warningIndex, child.getWarnings().get(warningIndex).getWarning());
            warningCounter++;
        }
        args.putInt("warningCounter", warningCounter);
        int accidentCounter = 0;
        for (int accidentIndex = 0; accidentIndex<child.getAccidents().size(); accidentIndex++)
        {
            args.putString("accident"+ accidentIndex, child.getAccidents().get(accidentIndex).getAccident());
            accidentCounter++;
        }
        args.putInt("accidentCounter", accidentCounter);
        int incidentCounter = 0;
        for (int incidentIndex = 0; incidentIndex<child.getIncidents().size(); incidentIndex++)
        {
            args.putString("incident"+ incidentIndex, child.getIncidents().get(incidentIndex).getIncident());
            incidentCounter++;
        }
        args.putInt("incidentCounter", incidentCounter);
        int contactCounter = 0;
        for (int contactIndex = 0; contactIndex<child.getContacts().size(); contactIndex++)
        {
            args.putString("contact"+ contactIndex, child.getContacts().get(contactIndex).getContactDetails());
            contactCounter++;
        }
        args.putInt("contactCounter", contactCounter);
        args.putString("dateOfBirth", child.getDateOfBirth());
        int allergiesCounter = 0;
        for (int allergiesIndex=0; allergiesIndex<child.getAllergies().size(); allergiesIndex++)
        {
            args.putString("allergy"+ allergiesIndex, child.getAllergies().get(allergiesIndex));
            allergiesCounter++;
        }
        args.putInt("allergiesCounter", allergiesCounter);
        int medicalCounter = 0;
        for (int medicalIndex=0; medicalIndex<child.getMedical().size(); medicalIndex++)
        {
            args.putString("medical"+ medicalIndex, child.getMedical().get(medicalIndex));
            medicalCounter++;
        }
        args.putInt("medicalCounter", medicalCounter);
        int clubCounter = 0;
        for (int clubIndex=0; clubIndex<child.getClubs().size(); clubIndex++)
        {
            args.putString("club"+ clubIndex, child.getClubs().get(clubIndex));
            clubCounter++;
        }
        args.putInt("clubCounter", clubCounter);
        return args;
    }
}
