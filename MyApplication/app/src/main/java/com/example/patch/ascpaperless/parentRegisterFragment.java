package com.example.patch.ascpaperless;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by David Marchant on 08/03/2016.
 *
 * This is the java for the parent facing register fragment. it should show a selectable register
 * of all the children currently in the club and allows parents to sign them out using a drawable
 * view
 */
public class parentRegisterFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.parent_register_fragment, container, false);
    }

    public void fillRegister(final ArrayList<child> children)
    {
        LinearLayout register = (LinearLayout)getView().findViewById(R.id.parentRegisterList);
        register.removeAllViews();

        for (int childIndex=0; childIndex<children.size(); childIndex++)
        {
            final child child = children.get(childIndex);
            if (child.getInTheClub())
            {
                // add the child's name
                final TextView name = new TextView(getActivity());
                name.setText(child.getFullName());
                name.setTextSize(getResources().getDimension(R.dimen.NameFontSize));
                name.setContentDescription(child.getIdentifier());
                name.setClickable(true);
                register.addView(name);

                // add a click listener for the name
                name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // clear the layout of anything and setup variables
                        LinearLayout infoPanel = (LinearLayout) getActivity().findViewById(R.id.childParentInfo);
                        infoPanel.removeAllViews();

                        // setup a new title
                        TextView childName = new TextView(getActivity());
                        childName.setText(child.getFullName());
                        childName.setTextSize(getResources().getDimension(R.dimen.parentTitleFontSize));
                        infoPanel.addView(childName);

                        // add a line under the title
                        View divider = new View(getActivity());
                        divider.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1));
                        divider.setBackgroundColor(getResources().getColor(R.color.divider));
                        infoPanel.addView(divider);

                        ScrollView scrollView = new ScrollView(getActivity());
                        LinearLayout scrollContainer = new LinearLayout(getActivity());
                        scrollContainer.setOrientation(LinearLayout.VERTICAL);

                        if (child.getPoints().size() > 0)
                        {
                            TextView pointTitle = new TextView(getActivity());
                            String pointsBuffer = "Points earned today";
                            pointTitle.setText(pointsBuffer);
                            pointTitle.setGravity(Gravity.END);
                            pointTitle.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                            scrollContainer.addView(pointTitle);

                            for (int pointIndex = 0; pointIndex < child.getPoints().size(); pointIndex++)
                            {
                                // add some text about points earned
                                TextView point = new TextView(getActivity());
                                String pointBuffer = "- "+ child.getPoints().get(pointIndex).getReason();
                                point.setText(pointBuffer);
                                point.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                                scrollContainer.addView(point);
                            }
                        }

                        if (child.getWarnings().size() > 0)
                        {
                            TextView pointTitle = new TextView(getActivity());
                            String pointTitleBuffer = "Warnings received earned today";
                            pointTitle.setText(pointTitleBuffer);
                            pointTitle.setGravity(Gravity.RIGHT);
                            pointTitle.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                            scrollContainer.addView(pointTitle);

                            for (int warningIndex = 0; warningIndex < child.getWarnings().size(); warningIndex++)
                            {
                                // add some text about points earned
                                TextView warning = new TextView(getActivity());
                                String warningBuffer = "- "+ child.getWarnings().get(warningIndex).getOffence();
                                warning.setText(warningBuffer);
                                warning.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                                scrollContainer.addView(warning);
                            }
                        }

                        if (child.getAccidents().size() > 0)
                        {
                            TextView pointTitle = new TextView(getActivity());
                            pointTitle.setText("Accidents");
                            pointTitle.setGravity(Gravity.RIGHT);
                            pointTitle.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                            scrollContainer.addView(pointTitle);

                            for (int accidentsIndex = 0; accidentsIndex < child.getAccidents().size(); accidentsIndex++)
                            {
                                // add some text about points earned
                                TextView accident = new TextView(getActivity());
                                String accidentBuffer = "- "+ child.getAccidents().get(accidentsIndex).getDescription();
                                accident.setText(accidentBuffer);
                                accident.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                                scrollContainer.addView(accident);
                            }
                        }

                        if (child.getIncidents().size() > 0)
                        {
                            TextView pointTitle = new TextView(getActivity());
                            pointTitle.setText("Incidents");
                            pointTitle.setGravity(Gravity.RIGHT);
                            pointTitle.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                            scrollContainer.addView(pointTitle);

                            for (int incidentIndex = 0; incidentIndex < child.getIncidents().size(); incidentIndex++)
                            {
                                // add some text about points earned
                                TextView incident = new TextView(getActivity());
                                String incidentBuffer = "- "+ child.getIncidents().get(incidentIndex).getDescription();
                                incident.setText(incidentBuffer);
                                incident.setTextSize(getResources().getDimension(R.dimen.parentFontSize));
                                scrollContainer.addView(incident);
                            }
                        }
                        scrollView.addView(scrollContainer);
                        infoPanel.addView(scrollView);

                        // try and add a custom signature
                        final SignatureView signatureView = new SignatureView(getActivity());
                        infoPanel.addView(signatureView);

                        // add the buttons
                        LinearLayout linearLayout = new LinearLayout(getActivity());
                        Button saveBtn = new Button(getActivity());
                        Button clearBtn = new Button(getActivity());
                        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        linearLayout.setGravity(Gravity.RIGHT);
                        saveBtn.setText("Confirm & Sign Out");
                        saveBtn.setTag("Save");
                        saveBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                String fileName = generateFileName(child.getIdentifier());
                                saveImage(signatureView.getSignature(), fileName);
                                child.signOut(fileName);
                                confirmSignature(child.getFullName());
                            }
                        });
                        clearBtn.setText("Clear Signature");
                        clearBtn.setTag("Clear");
                        clearBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                signatureView.clearSignature();
                            }
                        });
                        linearLayout.addView(clearBtn);
                        linearLayout.addView(saveBtn);
                        infoPanel.addView(linearLayout);
                    }
                });
            }
        }
    }

    private void confirmSignature(String name)
    {
        LinearLayout register = (LinearLayout) getActivity().findViewById(R.id.childParentInfo);

        TextView thanks = new TextView(getActivity());
        String thanksBuffer = "You have signed out "+ name;
        thanks.setText(thanksBuffer);
        thanks.setTextSize(getResources().getDimension(R.dimen.parentFontSize));

        register.addView(thanks);
    }


    private class SignatureView extends View
    {

        // set the stroke width
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();
        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public SignatureView(Context context) {

            super(context);

            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);

            // set the bg color as white
            this.setBackgroundColor(getResources().getColor(R.color.signature));

            // width and height should cover the screen
            this.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 300));
        }

        protected Bitmap getSignature() {

            Bitmap signatureBitmap;
            signatureBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.RGB_565);

            // important for saving signature
            final Canvas canvas = new Canvas(signatureBitmap);
            this.draw(canvas);

            return signatureBitmap;
        }

        private void clearSignature() {
            path.reset();
            this.invalidate();
        }

        // all touch events during the drawing
        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(this.path, this.paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:

                    path.moveTo(eventX, eventY);

                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);

                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:

                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }

    }

    /**
     * save the signature to an sd card directory
     *
     * @param signature bitmap
     */

    final void saveImage(Bitmap signature, String fileName)
    {
        String root = Environment.getExternalStorageDirectory().toString();

        // the directory where the signature will be saved
        File myDir = new File(root + "/saved_signature");

        // make the directory if it does not exist yet
        if (!myDir.exists())
        {
            myDir.mkdirs();
        }

        // in our case, we delete the previous file, you can remove this
        File file = new File(myDir, fileName);
        if (file.exists())
        {
            file.delete();
        }

        try
        {
            // save the signature
            FileOutputStream out = new FileOutputStream(file);
            signature.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String cleanSignature(String stringToClean)
    {
        stringToClean = stringToClean.replace(" ", "-");
        stringToClean = stringToClean.replace("/", "-");
        return stringToClean;
    }

    public String generateFileName(String identifier)
    {
        Calendar calendar = Calendar.getInstance();
        return cleanSignature(identifier + calendar.getTime()) +".png";
    }
}
