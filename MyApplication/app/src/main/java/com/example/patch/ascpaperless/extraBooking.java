package com.example.patch.ascpaperless;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

/**
 * Created by David Marchant on 29/03/2016.
 *
 * This is the java for the extra booking fragment
 */
public class extraBooking extends DialogFragment
{
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.extra_booking, container, false);
        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());

        String title = "Extra Booking";
        getDialog().setTitle(title);

        ArrayList<child> allChildren = clubDay.getRegister();
        ArrayList<child> childrenNotInTheClub = new ArrayList<>();
        for (int childIndex = 0; childIndex<allChildren.size(); childIndex++)
        {
            if (allChildren.get(childIndex).getBooked().equals("absent"))
            {
                childrenNotInTheClub.add(allChildren.get(childIndex));
            }
        }

        final GridView gridView = (GridView)view.findViewById(R.id.staffGrid);
        gridView.setAdapter(new ExtraBookingAdapter(getActivity(), childrenNotInTheClub));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ((MainActivity) getActivity()).extraBooking(gridView.getChildAt(position).getContentDescription().toString());
            }
        });
        return view;
    }
}
