package com.example.patch.ascpaperless;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

/**
 * Created by David Marchant on 29/03/2016.
 *
 * This is the java for the staff absence dialog.
 */
public class staffAbsence extends DialogFragment
{
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.staff_absence, container, false);
        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());

        String title = "Staff Absence";
        getDialog().setTitle(title);

        ArrayList<staff> allStaff = clubDay.getStaffTeam();
        ArrayList<staff> staffInTheClub = new ArrayList<>();
        for (int staffIndex = 0; staffIndex<allStaff.size(); staffIndex++)
        {
            if (allStaff.get(staffIndex).getWorking().equals("in") ||
                allStaff.get(staffIndex).getWorking().equals("replacement"))
            {
                staffInTheClub.add(allStaff.get(staffIndex));
            }
        }

        final GridView gridView = (GridView)view.findViewById(R.id.staffGrid);
        gridView.setAdapter(new StaffAbsenceAdapter(getActivity(), staffInTheClub));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
            {
                ((MainActivity) getActivity()).staffAbsence(gridView.getChildAt(position).getContentDescription().toString());
            }
        });

        return view;
    }
}