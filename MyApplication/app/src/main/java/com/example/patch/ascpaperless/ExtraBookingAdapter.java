package com.example.patch.ascpaperless;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by David Marchant on 29/03/2016.
 *
 * This is a custom adapter allowing for the population of the extra booking dialog
 */
public class ExtraBookingAdapter extends BaseAdapter
{
    private Context mContext;
    private ArrayList<child> register;

    public ExtraBookingAdapter(Context c, ArrayList<child> r)
    {
        mContext = c;
        register = r;
    }

    public int getCount()
    {
        return register.size();
    }

    public Object getItem(int position)
    {
        return null;
    }

    public long getItemId(int position)
    {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {

        TextView button;
        if (convertView == null)
        {
            button = new TextView(mContext);
            String buttonTextBuffer = "buttonTest";
            button.setText(buttonTextBuffer);
        }
        else
        {
            button = (TextView) convertView;
        }

        button.setText(register.get(position).getFullName());
        button.setContentDescription(register.get(position).getIdentifier());
        button.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        button.setTextSize(parent.getResources().getDimension(R.dimen.otherFontSize));

        return button;
    }
}
