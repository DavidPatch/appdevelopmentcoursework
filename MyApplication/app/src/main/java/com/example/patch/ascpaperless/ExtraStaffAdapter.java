package com.example.patch.ascpaperless;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by David Marchant on 29/03/2016.
 *
 * This is a custom adapter for the extra staff dialog and allows for it to be populated
 */
public class ExtraStaffAdapter extends BaseAdapter
{
    private Context mContext;
    private ArrayList<staff> staffTeam;

    public ExtraStaffAdapter(Context c, ArrayList<staff> s)
    {
        mContext = c;
        staffTeam = s;
    }

    public int getCount()
    {
        return staffTeam.size();
    }

    public Object getItem(int position)
    {
        return null;
    }

    public long getItemId(int position)
    {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {

        TextView button;
        if (convertView == null)
        {
            button = new TextView(mContext);
            String buttonTextBuffer = "buttonTest";
            button.setText(buttonTextBuffer);
        }
        else
        {
            button = (TextView) convertView;
        }

        button.setText(staffTeam.get(position).getFullName());
        button.setContentDescription(staffTeam.get(position).getFullName());
        button.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        button.setTextSize(parent.getResources().getDimension(R.dimen.otherFontSize));

        return button;
    }
}
