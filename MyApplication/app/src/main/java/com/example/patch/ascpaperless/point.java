package com.example.patch.ascpaperless;

/**
 * Created by David Marchant on 23/03/2016.
 *
 * This is the class description for the point class
 */
public class point
{
    private String staff;
    private String reason;

    public void setStaff(String staff)
    {
        if (staff != null)
        {
            this.staff = staff;
        }
        else
        {
            throw new ClassCastException("Staff name cannot cannot be null");
        }
    }

    public String getStaff()
    {
        return staff;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getReason()
    {
        return reason;
    }

    public String getPoint()
    {
        return getStaff() +" - "+ getReason();
    }

    public point(String staff, String description)
    {
        setStaff(staff);
        setReason(description);
    }
}
