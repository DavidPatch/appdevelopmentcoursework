package com.example.patch.ascpaperless;

import org.json.JSONObject;

/**
 * Created by David Marchant on 15/03/2016.
 *
 * This is the class description for the staff class
 */
public class staff
{
    private String firstName;
    private String surname;
    private String jobTitle;
    private String working;

    public void setFirstName(String firstName)
    {
        if (firstName != null)
        {
            this.firstName = firstName;
        }
        else
        {
            throw new ClassCastException("Staff first name cannot be null");
        }
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setSurname(String surname)
    {
        if (surname != null)
        {
            this.surname = surname;
        }
        else
        {
            throw new ClassCastException("Staff surname cannot be null");
        }
    }

    public String getSurname()
    {
        return surname;
    }

    public void setJobTitle(String jobTitle)
    {
        if (jobTitle != null)
        {
            this.jobTitle = jobTitle;
        }
        else
        {
            throw new ClassCastException("Staff job title cannot be null");
        }
    }

    public String getJobTitle()
    {
        return jobTitle;
    }

    public void setWorking(String working)
    {
        if (working.equals("in") ||
            working.equals("holiday") ||
            working.equals("ill") ||
            working.equals("replacement"))
        {
            this.working = working;
        }
        else
        {
            throw new ClassCastException("Staff can only be listed as 'in', 'holiday', 'ill', or 'replacement'");
        }
    }

    public String getWorking()
    {
        return working;
    }

    public String getFullName()
    {
        return getFirstName() +" "+ getSurname();
    }

    public staff(JSONObject staffMember)
    {
        setFirstName(staffMember.optString("firstName"));
        setSurname(staffMember.optString("surname"));
        setJobTitle(staffMember.optString("jobTitle"));
        setWorking(staffMember.optString("working"));
    }
}
