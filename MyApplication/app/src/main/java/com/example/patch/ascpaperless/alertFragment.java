package com.example.patch.ascpaperless;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by David Marchant on 08/03/2016.
 *
 * This is the code for the alert fragment. It generates alerts and contains buttons for adding or
 * removing staff and children. Also displays the staff Ratio
 */
public class alertFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.alert_fragment, container, false);

        // add event for clicking extra booking button
        Button buttonExtraBooking = (Button) view.findViewById(R.id.extraBooking);
        buttonExtraBooking.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((MainActivity) getActivity()).displayExtraBooking();
            }
        });

        // add event for clicking cancellation button
        Button buttonCancellation = (Button) view.findViewById(R.id.cancellation);
        buttonCancellation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((MainActivity) getActivity()).displayCancellation();
            }
        });

        // add event for clicking extra staff button
        Button buttonExtraStaff = (Button) view.findViewById(R.id.extraStaff);
        buttonExtraStaff.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((MainActivity) getActivity()).displayExtraStaff();
            }
        });

        // add event for clicking staff absence button
        Button buttonStaffAbsence = (Button) view.findViewById(R.id.cancelledStaff);
        buttonStaffAbsence.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((MainActivity) getActivity()).displayStaffAbsence();
            }
        });

        return view;
    }

    // generate all the alerts. Go through the clubs register and display any important information
    // colour coded so they can be easily matched up to the register alerts. This method has ended up
    // extremely long as the references to the resources cannot seemingly be looped through easilly.
    public void generateAlerts(ArrayList<child> children, ArrayList<staff> staffTeam, ArrayList<club> clubs)
    {
        TableLayout alertTable = (TableLayout)getView().findViewById(R.id.alertTable);
        alertTable.removeAllViews();

        // Setup the alerts for the day
        String allergyAlerts = "";
        String medicalAlerts = "";
        String additionalAlerts = "";
        String extraAlerts = "";
        String absentAlerts = "";
        String offAlerts = "";
        String replacementAlerts = "";
        String pointsAlerts = "";
        String warningsAlerts = "";
        String accidentsAlerts = "";
        String incidentsAlerts = "";

        // start updating the alerts
        TextView staffRatio = (TextView)getView().findViewById(R.id.Ratio);
        int childCounter = 0;
        int staffCounter = 0;
        for (int staffIndex=0; staffIndex<staffTeam.size(); staffIndex++)
        {
            if (staffTeam.get(staffIndex).getWorking().equals("in"))
            {
                staffCounter++;
            }
            if (staffTeam.get(staffIndex).getWorking().equals("replacement"))
            {
                staffCounter++;
                if (replacementAlerts.equals(""))
                {
                    replacementAlerts = staffTeam.get(staffIndex).getFullName();
                }
                else
                {
                    replacementAlerts = replacementAlerts +",\n"+ staffTeam.get(staffIndex).getFullName();
                }
            }
            if (staffTeam.get(staffIndex).getWorking().equals("holiday") || staffTeam.get(staffIndex).getWorking().equals("ill"))
            {
                if (offAlerts.equals(""))
                {
                    offAlerts = staffTeam.get(staffIndex).getFullName();
                }
                else
                {
                    offAlerts = offAlerts +",\n"+ staffTeam.get(staffIndex).getFullName();
                }
            }
        }
        for (int childIndex=0; childIndex<children.size(); childIndex++)
        {
            String fullName = children.get(childIndex).getFullName();

            if (children.get(childIndex).getBooked().equals("regular"))
            {
                childCounter++;
            }
            if (children.get(childIndex).getBooked().equals("extra"))
            {
                childCounter++;
                if (extraAlerts.equals(""))
                {
                    extraAlerts = fullName;
                }
                else
                {
                    extraAlerts = extraAlerts +",\n"+ fullName;
                }
            }
            if (children.get(childIndex).getBooked().equals("absent"))
            {
                if (absentAlerts.equals(""))
                {
                    absentAlerts = fullName;
                }
                else
                {
                    absentAlerts = absentAlerts +",\n"+ fullName;
                }
            }

            if ((children.get(childIndex).getMedical().size() >= 1) && (!children.get(childIndex).getBooked().equals("absent")))
            {
                ArrayList<String> medicalNeeds = children.get(childIndex).getMedical();
                String medicalList = "";
                for (int medicalIndex = 0; medicalIndex < medicalNeeds.size(); medicalIndex++)
                {
                    if (medicalList.equals(""))
                    {
                        medicalList = medicalNeeds.get(medicalIndex);
                    }
                    else
                    {
                        medicalList = medicalList +", "+ medicalNeeds.get(medicalIndex);
                    }
                }
                if (!medicalList.equals(""))
                {
                    if (medicalAlerts.equals(""))
                    {
                        medicalAlerts = fullName +": "+ medicalList;
                    }
                    else
                    {
                        medicalAlerts = medicalAlerts +"\n"+ fullName +": "+ medicalList;
                    }
                }
            }

            if ((children.get(childIndex).getAllergies().size() >= 1) && (!children.get(childIndex).getBooked().equals("absent")))
            {
                ArrayList<String> allAllergies = children.get(childIndex).getAllergies();
                String allergyList = "";
                for (int allergyIndex = 0; allergyIndex < allAllergies.size(); allergyIndex++)
                {
                    if (allergyList.equals(""))
                    {
                        allergyList = allAllergies.get(allergyIndex);
                    }
                    else
                    {
                        allergyList = allergyList +", "+ allAllergies.get(allergyIndex);
                    }
                }
                if (!allergyList.equals(""))
                {
                    if (allergyAlerts.equals(""))
                    {
                        allergyAlerts = fullName +": "+ allergyList;
                    }
                    else
                    {
                        allergyAlerts = allergyAlerts +"\n"+ fullName +": "+ allergyList;
                    }
                }
            }

            if ((!children.get(childIndex).getAdditional().equals("None")) && (!children.get(childIndex).getBooked().equals("absent")))
            {
                if (additionalAlerts.equals(""))
                {
                    additionalAlerts = fullName +": "+ children.get(childIndex).getAdditional();
                }
                else
                {
                    additionalAlerts = additionalAlerts +"\n"+ fullName +": "+ children.get(childIndex).getAdditional();
                }
            }

            if ((children.get(childIndex).getPoints().size() >= 1) && (!children.get(childIndex).getBooked().equals("absent")))
            {
                ArrayList<point> allPoints = children.get(childIndex).getPoints();
                String pointList = "";
                for (int pointIndex = 0; pointIndex < allPoints.size(); pointIndex++)
                {
                    if (pointList.equals(""))
                    {
                        pointList = allPoints.get(pointIndex).getPoint();
                    }
                    else
                    {
                        pointList = pointList +", "+ allPoints.get(pointIndex).getPoint();
                    }
                }
                if (!pointList.equals(""))
                {
                    if (pointsAlerts.equals(""))
                    {
                        pointsAlerts = fullName +": "+ pointList;
                    }
                    else
                    {
                        pointsAlerts = pointsAlerts +"\n"+ fullName +": "+ pointList;
                    }
                }
            }

            if ((children.get(childIndex).getWarnings().size() >= 1) && (!children.get(childIndex).getBooked().equals("absent")))
            {
                ArrayList<warning> allWarnings = children.get(childIndex).getWarnings();
                String warningList = "";
                for (int warningIndex = 0; warningIndex < allWarnings.size(); warningIndex++)
                {
                    if (warningList.equals(""))
                    {
                        warningList = allWarnings.get(warningIndex).getWarning();
                    }
                    else
                    {
                        warningList = warningList +", "+ allWarnings.get(warningIndex).getWarning();
                    }
                }
                if (!warningList.equals(""))
                {
                    if (warningsAlerts.equals(""))
                    {
                        warningsAlerts = fullName +": "+ warningList;
                    }
                    else
                    {
                        warningsAlerts = pointsAlerts +"\n"+ fullName +": "+ warningList;
                    }
                }
            }

            if ((children.get(childIndex).getIncidents().size() >= 1) && (!children.get(childIndex).getBooked().equals("absent")))
            {
                ArrayList<incident> allIncidents = children.get(childIndex).getIncidents();
                String incidentList = "";
                for (int incidentIndex = 0; incidentIndex < allIncidents.size(); incidentIndex++)
                {
                    if (incidentList.equals(""))
                    {
                        incidentList = allIncidents.get(incidentIndex).getIncident();
                    }
                    else
                    {
                        incidentList = incidentList +", "+ allIncidents.get(incidentIndex).getIncident();
                    }
                }
                if (!incidentList.equals(""))
                {
                    if (incidentsAlerts.equals(""))
                    {
                        incidentsAlerts = fullName +": "+ incidentList;
                    }
                    else
                    {
                        incidentsAlerts = incidentsAlerts +"\n"+ fullName +": "+ incidentList;
                    }
                }
            }

            if ((children.get(childIndex).getAccidents().size() >= 1) && (!children.get(childIndex).getBooked().equals("absent")))
            {
                ArrayList<accident> allAccidents = children.get(childIndex).getAccidents();
                String accidentList = "";
                for (int accidentIndex = 0; accidentIndex < allAccidents.size(); accidentIndex++)
                {
                    if (accidentList.equals(""))
                    {
                        accidentList = allAccidents.get(accidentIndex).getAccident();
                    }
                    else
                    {
                        accidentList = accidentList +", "+ allAccidents.get(accidentIndex).getAccident();
                    }
                }
                if (!accidentList.equals(""))
                {
                    if (accidentsAlerts.equals(""))
                    {
                        accidentsAlerts = fullName +": "+ accidentList;
                    }
                    else
                    {
                        accidentsAlerts = accidentsAlerts +"\n"+ fullName +": "+ accidentList;
                    }
                }
            }
        }

        TableRow extraRow = new TableRow(getActivity());
        TextView extraTitle = new TextView(getActivity());
        TextView extraAlert = new TextView(getActivity());
        extraTitle.setText(getResources().getString(R.string.extraTitle));
        extraTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        extraAlert.setText(extraAlerts);
        extraAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        extraRow.setBackgroundColor(getResources().getColor(R.color.extra));
        extraRow.addView(extraTitle);
        extraRow.addView(extraAlert);
        alertTable.addView(extraRow);

        TableRow absentRow = new TableRow(getActivity());
        TextView absentTitle = new TextView(getActivity());
        TextView absentAlert = new TextView(getActivity());
        absentTitle.setText(getResources().getString(R.string.absentTitle));
        absentTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        absentAlert.setText(absentAlerts);
        absentAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        absentRow.setBackgroundColor(getResources().getColor(R.color.absent));
        absentRow.addView(absentTitle);
        absentRow.addView(absentAlert);
        alertTable.addView(absentRow);

        TableRow offRow = new TableRow(getActivity());
        TextView offTitle = new TextView(getActivity());
        TextView offAlert = new TextView(getActivity());
        offTitle.setText(getResources().getString(R.string.offTitle));
        offTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        offAlert.setText(offAlerts);
        offAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        offRow.setBackgroundColor(getResources().getColor(R.color.off));
        offRow.addView(offTitle);
        offRow.addView(offAlert);
        alertTable.addView(offRow);

        TableRow replacementRow = new TableRow(getActivity());
        TextView replacementTitle = new TextView(getActivity());
        TextView replacementAlert = new TextView(getActivity());
        replacementTitle.setText(getResources().getString(R.string.replacementTitle));
        replacementTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        replacementAlert.setText(replacementAlerts);
        replacementAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        replacementRow.setBackgroundColor(getResources().getColor(R.color.replacement));
        replacementRow.addView(replacementTitle);
        replacementRow.addView(replacementAlert);
        alertTable.addView(replacementRow);

        TableRow additionalRow = new TableRow(getActivity());
        TextView additionalTitle = new TextView(getActivity());
        TextView additionalAlert = new TextView(getActivity());
        additionalTitle.setText(getResources().getString(R.string.additionalTitle));
        additionalTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        additionalAlert.setText(additionalAlerts);
        additionalAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        additionalRow.setBackgroundColor(getResources().getColor(R.color.additional));
        additionalRow.addView(additionalTitle);
        additionalRow.addView(additionalAlert);
        alertTable.addView(additionalRow);

        TableRow allergyRow = new TableRow(getActivity());
        TextView allergyTitle = new TextView(getActivity());
        TextView allergyAlert = new TextView(getActivity());
        allergyTitle.setText(getResources().getString(R.string.allergyTitle));
        allergyTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        allergyAlert.setText(allergyAlerts);
        allergyAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        allergyRow.setBackgroundColor(getResources().getColor(R.color.allergies));
        allergyRow.addView(allergyTitle);
        allergyRow.addView(allergyAlert);
        alertTable.addView(allergyRow);

        TableRow medicalRow = new TableRow(getActivity());
        TextView medicalTitle = new TextView(getActivity());
        TextView medicalAlert = new TextView(getActivity());
        medicalTitle.setText(getResources().getString(R.string.medicalTitle));
        medicalTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        medicalAlert.setText(medicalAlerts);
        medicalAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        medicalRow.setBackgroundColor(getResources().getColor(R.color.medical));
        medicalRow.addView(medicalTitle);
        medicalRow.addView(medicalAlert);
        alertTable.addView(medicalRow);

        for (int clubIndex=0; clubIndex<clubs.size(); clubIndex++)
        {
            String clubChildren = "";
            String clubName = clubs.get(clubIndex).getTitle();
            String clubStart = clubs.get(clubIndex).getStartAt();
            String clubFinish = clubs.get(clubIndex).getEndAt();
            for (int childIndex=0; childIndex<children.size(); childIndex++)
            {
                child child = children.get(childIndex);
                String fullName = child.getFullName();
                {
                    if ((child.getClubs().contains(clubName) && (!children.get(childIndex).getBooked().equals("absent"))))
                    {
                        if (clubChildren.equals(""))
                        {
                            clubChildren = fullName;
                        }
                        else
                        {
                            clubChildren = clubChildren +",\n"+ fullName;
                        }
                    }
                }
            }
            TableRow clubRow = new TableRow(getActivity());
            TextView clubTitle = new TextView(getActivity());
            TextView clubAlert = new TextView(getActivity());
            String clubInfo = clubName +":\n"+ clubStart +" to "+ clubFinish;
            clubTitle.setText(clubInfo);
            clubTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
            clubAlert.setText(clubChildren);
            clubAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
            clubRow.setBackgroundColor(getResources().getColor(R.color.clubs));
            clubRow.addView(clubTitle);
            clubRow.addView(clubAlert);
            alertTable.addView(clubRow);
        }

        TableRow pointRow = new TableRow(getActivity());
        TextView pointTitle = new TextView(getActivity());
        TextView pointAlert = new TextView(getActivity());
        pointTitle.setText(getResources().getString(R.string.pointTitle));
        pointTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        pointAlert.setText(pointsAlerts);
        pointAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        pointRow.setBackgroundColor(getResources().getColor(R.color.points));
        pointRow.addView(pointTitle);
        pointRow.addView(pointAlert);
        alertTable.addView(pointRow);

        TableRow warningRow = new TableRow(getActivity());
        TextView warningTitle = new TextView(getActivity());
        TextView warningAlert = new TextView(getActivity());
        warningTitle.setText(getResources().getString(R.string.warningTitle));
        warningTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        warningAlert.setText(warningsAlerts);
        warningAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        warningRow.setBackgroundColor(getResources().getColor(R.color.warnings));
        warningRow.addView(warningTitle);
        warningRow.addView(warningAlert);
        alertTable.addView(warningRow);

        TableRow accidentRow = new TableRow(getActivity());
        TextView accidentTitle = new TextView(getActivity());
        TextView accidentAlert = new TextView(getActivity());
        accidentTitle.setText(getResources().getString(R.string.accidentTitle));
        accidentTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        accidentAlert.setText(accidentsAlerts);
        accidentAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        accidentRow.setBackgroundColor(getResources().getColor(R.color.accidents));
        accidentRow.addView(accidentTitle);
        accidentRow.addView(accidentAlert);
        alertTable.addView(accidentRow);

        TableRow incidentRow = new TableRow(getActivity());
        TextView incidentTitle = new TextView(getActivity());
        TextView incidentAlert = new TextView(getActivity());
        incidentTitle.setText(getResources().getString(R.string.incidentTitle));
        incidentTitle.setTextSize(getResources().getDimension(R.dimen.TitleFontSize));
        incidentAlert.setText(incidentsAlerts);
        incidentAlert.setTextSize(getResources().getDimension(R.dimen.AlertFontSize));
        incidentRow.setBackgroundColor(getResources().getColor(R.color.incidents));
        incidentRow.addView(incidentTitle);
        incidentRow.addView(incidentAlert);
        alertTable.addView(incidentRow);

        // calculate staff ratio
        staffRatio.setText(calculateRatio(childCounter, staffCounter));
        int staffNumber = Integer.parseInt(staffRatio.getText().toString().substring(2));
        if (staffNumber <= 8)
        {
            staffRatio.setTextColor(Color.rgb(0, 102, 0));
        }
        else if (staffNumber <= 10)
        {
            staffRatio.setTextColor(Color.rgb(0, 0, 255));
        }
        else
        {
            staffRatio.setTextColor(Color.rgb(255, 0, 0));
        }
    }

    // calculate the staff ratio. important to note that remainders in the ratio mean lower ratio
    public String calculateRatio(int children, int staff)
    {
        int ratio;
        if (children % staff == 0)
        {
            ratio = children/staff;
        }
        else
        {
            ratio = (children/staff)+1;
        }
        return "1:"+ ratio;
    }
}