package com.example.patch.ascpaperless;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

/**
 * Created by David Marchant on 29/03/2016.
 *
 * This is the class description for the cancellation dialog, which displays from the cancellation
 * button in the alert fragment
 */
public class cancellation extends DialogFragment
{
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.cancellation, container, false);
        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());

        String title = "Cancellation";
        getDialog().setTitle(title);

        ArrayList<child> allChildren = clubDay.getRegister();
        ArrayList<child> childrenInTheClub = new ArrayList<>();
        for (int childIndex = 0; childIndex<allChildren.size(); childIndex++)
        {
            if (allChildren.get(childIndex).getBooked().equals("regular") ||
                allChildren.get(childIndex).getBooked().equals("extra"))
            {
                childrenInTheClub.add(allChildren.get(childIndex));
            }
        }

        final GridView gridView = (GridView)view.findViewById(R.id.staffGrid);
        gridView.setAdapter(new CancellationAdapter(getActivity(), childrenInTheClub));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
            {
                ((MainActivity) getActivity()).cancellation(gridView.getChildAt(position).getContentDescription().toString());
            }
        });

        return view;
    }
}