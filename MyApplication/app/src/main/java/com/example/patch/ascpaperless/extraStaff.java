package com.example.patch.ascpaperless;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

/**
 * Created by David Marchant on 29/03/2016.
 *
 * This is the java for the extra staff dialog
 */
public class extraStaff extends DialogFragment
{
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.extra_staff, container, false);
        clubDay clubDay = ((clubDay)getActivity().getApplicationContext());

        String title = "Extra Staff";
        getDialog().setTitle(title);

        ArrayList<staff> allStaff = clubDay.getStaffTeam();
        ArrayList<staff> staffOutOfTheClub = new ArrayList<>();
        for (int staffIndex = 0; staffIndex<allStaff.size(); staffIndex++)
        {
            if (allStaff.get(staffIndex).getWorking().equals("holiday") ||
                allStaff.get(staffIndex).getWorking().equals("ill"))
            {
                staffOutOfTheClub.add(allStaff.get(staffIndex));
            }
        }

        final GridView gridView = (GridView)view.findViewById(R.id.staffGrid);
        gridView.setAdapter(new ExtraStaffAdapter(getActivity(), staffOutOfTheClub));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ((MainActivity) getActivity()).extraStaff(gridView.getChildAt(position).getContentDescription().toString());
            }
        });

        return view;
    }
}