package com.example.patch.ascpaperless;

import org.json.JSONObject;

/**
 * Created by David Marchant on 15/03/2016.
 *
 * This is the class description for a contact, which is part of the child class
 */
public class contact
{
    private String name;
    private String phoneNumber;

    public void setName(String name)
    {
        if (name != null)
        {
            this.name = name;
        }
        else
        {
            throw new ClassCastException("Contact name cannot be null");
        }
    }

    public String getName()
    {
        return name;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        if (phoneNumber != null)
        {
            this.phoneNumber = phoneNumber;
        }
        else
        {
            throw new ClassCastException("Contact phone number cannot be null");
        }
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getContactDetails()
    {
        return getName() +": "+ getPhoneNumber();
    }

    public contact(JSONObject contact)
    {
        setName(contact.optString("contact"));
        setPhoneNumber(contact.optString("phoneNumber"));
    }
}
