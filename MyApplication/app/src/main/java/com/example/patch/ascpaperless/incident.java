package com.example.patch.ascpaperless;

/**
 * Created by David Marchant on 23/03/2016.
 *
 * This is the class description for the incident class
 */
public class incident
{
    private String staff;
    private String description;

    public void setStaff(String staff)
    {
        if (staff != null)
        {
            this.staff = staff;
        }
        else
        {
            throw new ClassCastException("Staff name cannot cannot be null");
        }
    }

    public String getStaff()
    {
        return staff;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public String getIncident()
    {
        return getStaff() +" - "+ getDescription();
    }

    public incident(String staff, String description)
    {
        setStaff(staff);
        setDescription(description);
    }
}
